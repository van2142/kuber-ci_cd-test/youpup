import os
import sys
import pendulum
import socket
import traceback
from time import sleep
import smtplib
import html
#from os import spawn*
import subprocess


import datetime

from airflow import DAG
from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator
from airflow.operators.email import EmailOperator
from airflow.utils.dates import days_ago
from airflow.utils.email import send_email
from airflow.hooks.base_hook import BaseHook
import airflow
from kubernetes.client import models as k8s

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__))))
from test_dag_code import foo

local_tz = pendulum.timezone("Europe/Moscow")

def send_mail(**kwargs):
    ls=os.listdir()
    print(ls)
    mail_to = kwargs["mailTo"]
    body = '''
    <html>
    <head>

    <style>

    p {
        color: black;
        font-size: 16;
    }

    </style>

    </head>

    <p>Добрый день</p>

    <p>Это тестовое сообщение от AirPlow</p>
    <br><img src="cid:airplov.jpg"><br>
    <p>Дата и время выполнения скрипта: ''' + datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S') + '''</p>
    <p>Сервер отправки: ''' + socket.gethostname() + '''</p>
    <br/>
    <br/>
    </html>'''

    subject = 'Тестовый скрипт отправки сообщений'

    send_email(mail_to, subject, body, mime_charset = 'utf-8')

# content='''
#     <html>
#     <head>

#     <style>

#     p {
#         color: black;
#         font-size: 16;
#     }

#     </style>

#     </head>

#     <p>Добрый день</p>

#     <p>Это тестовое сообщение от AirPlow</p>
#     <br><img src="cid:airplov.jpg"><br>
#     <p>Дата и время выполнения скрипта: ''' + datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S') + '''</p>
#     <p>Сервер отправки: ''' + socket.gethostname() + '''</p>
#     <br/>
#     <br/>
#     </html>'''



def generate_error():
    print("error in Dag")

    
default_args = {
    'owner': 'ikretinin',
    'start_date': days_ago(1).replace(tzinfo=local_tz),
    'retries': 0,
    'retry_delay': datetime.timedelta(minutes = 1),
    'email': 'van2142@yandex.ru',
    'email_on_retry': False,
    'email_on_fail': True,
    'on_failure_callback': generate_error()
}


with DAG('Mail_testing',
        default_args=default_args,
        description='test send_mail dag',
        schedule_interval= "@once",
        catchup=False
        ) as dag:
    
    run_email_dag = PythonOperator(
        task_id="run_email_dag",
        python_callable=send_mail,
        op_kwargs={
            "mailTo": "van2142@yandex.ru"
        }
    )
    
    # run_email_dag = EmailOperator(
    #     task_id="run_email_dag",
    #     files=['/airplov.jpg'],
    #     # provide_context=True,
    #     to="metrovan2033@gmail.com",
    #     subject='Тестовый скрипт отправки сообщений',
    #     html_content=content,
    #     dag=dag
    # )

    
# with DAG('TestDag_send_mail',
#          default_args=default_args,
#          description='test send_mail dag',
#          schedule_interval= "@hourly",
#          catchup=False
#         #  access_control={'DevOps_Crew' : {'can_read', 'can_edit'}}
#          ) as dag_mail:
    
#     run_email_dag = PythonOperator(
#         task_id="run_email_dag",
#         python_callable=send_mail,
#         op_kwargs={
#             "mailTo": ""
#         }
#     )
