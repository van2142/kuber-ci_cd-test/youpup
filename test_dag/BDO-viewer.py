import os
import sys
import pendulum
import socket
import traceback
from time import sleep
import smtplib
import html
#from os import spawn*
import subprocess

import datetime

from airflow import DAG
from airflow.models import Variable
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from airflow.utils.email import send_email
from airflow.hooks.base_hook import BaseHook
import airflow
from kubernetes.client import models as k8s

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__))))
# import taska
from taska import main as run_tsk
from test_dag_code import foo

local_tz = pendulum.timezone("Europe/Moscow")


def send_mail(**kwargs):
    mail_to = kwargs["mailTo"]
    body = '''
    <html>
    <head>

    <style>

    p {
        color: black;
        font-size: 16;
    }

    </style>

    </head>

    <p>Добрый день</p>

    <p>Результаты работы скрипта парсинга:</p>
    <p>Дата и время выполнения скрипта: ''' + datetime.datetime.now().strftime('%d.%m.%Y %H:%M:%S') + '''</p>
    <p>Сервер отправки: ''' + socket.gethostname() + '''</p>
    <br/>
    <br/>
    </html>'''

    subject = 'Тестовый скрипт отправки сообщений'

    send_email(mail_to, subject, body, mime_charset = 'utf-8')


def generate_error():
    print("error in Dag")

    
default_args = {
    'owner': 'ikretinin',
    # 'start_date': days_ago(1).replace(tzinfo=local_tz),
    'start_date': datetime.datetime(2022,1,2).astimezone(local_tz),
    'retries': 0,
    'retry_delay': datetime.timedelta(minutes = 1),
    'email': 'ikretinin@neoflex.ru',
    'email_on_retry': False,
    'email_on_fail': True,
    'on_failure_callback': generate_error()
}


with DAG('YouT_BDO',
         default_args=default_args,
         description='YouTube videos viewer',
        #  schedule_interval= "0 1,3,5,7,9,11,13,15,17,19,21,23 2 * *", ##"0 0 * 1 *",  ##0,1,2,3,4,5,6 0,2,4,6,8,10,12,14,16,18,20,22 * * *
         schedule_interval= "35 13 14 * *",
         catchup=False
         ) as dag:
    
    run_ytviewer_dag = PythonOperator(
        task_id="run_ytviewer_dag",
        python_callable=run_tsk,
        provide_context=True,
        executor_config = {
            # "pod_template_file": os.path.join(os.path.abspath(os.path.join(os.path.dirname(__file__))), "pod_in_dag_test.yml")
            # "KubernetesExecutor": {"image": "registry.gitlab.com/van2142/are_you_tubed:airflow_yt_v4_4"}
            #access_control={'DevOps_Crew' : {'can_read', 'can_edit'}}
            # "pod_template_file": os.path.join('/opt/airflow/pod_templates/pod_template.yaml'),
            "pod_override": k8s.V1Pod(spec=k8s.V1PodSpec(
                # node_selector={"kubernetes.io/hostname":"microkube01.van2142.cf"},
                containers=[
                    k8s.V1Container(
                        name="base",
                        image="registry.gitlab.com/van2142/are_you_tubed:airflow_yt_v4_4"
                    ),
                ],
                )
                ),
        },
    )

    
# with DAG('TestDag_send_mail',
#          default_args=default_args,
#          description='test send_mail dag',
#          schedule_interval= "@hourly",
#          catchup=False
#         #  access_control={'DevOps_Crew' : {'can_read', 'can_edit'}}
#          ) as dag_mail:
    
#     run_email_dag = PythonOperator(
#         task_id="run_email_dag",
#         python_callable=send_mail,
#         op_kwargs={
#             "mailTo": ""
#         }
#     )
run_ytviewer_dag